#!/bin/bash

REALPATH=`realpath $0`
REALPATH=`dirname $REALPATH`
DEFAULTNAME=torjail
NAME=$DEFAULTNAME
USERNAME=${SUDO_USER:-$(whoami)}
VERBOSE=
KEEPRESOLVE=
IPHOSTDEFAULT=10.200.1.1
IPHOST=$IPHOSTDEFAULT
IPNETNSDEFAULT=10.200.1.2
IPNETNS=$IPNETNSDEFAULT
NETMASKDEFAULT=24
NETMASK=$NETMASKDEFAULT

# Functions
# ~~~~~~~~~

print_real() {
  if [ "$VERBOSE" != y ]; then
    return
  fi

  if [ $2 = 'G' ]; then
    echo $1 -e "\e[32m$3\e[0m"
  elif [ $2 = 'Y' ]; then
    echo $1 -e "\e[33m$3\e[0m"
  elif [ $2 = 'N' ]; then
    echo $1 "$3"
  else
    echo $1 -e "\e[31m$3\e[0m"
  fi
}

print() {
  print_real '' "$1" "$2"
}

printn() {
  print_real "-n" "$1" "$2"
}

printv() {
  OLDVERBOSE=$VERBOSE
  VERBOSE=y
  print_real '' "$1" "$2"
  VERBOSE=$OLDVERBOSE
}

printvn() {
  OLDVERBOSE=$VERBOSE
  VERBOSE=y
  print_real "-n" "$1" "$2"
  VERBOSE=$OLDVERBOSE
}

help_and_exit() {
  VERBOSE=y
  printn N "Usage: "
  printn Y "$DEFAULTNAME"
  print G " <options> [command <arguments>...]"
  print N "Options:"
  print N "    -h, --help         It shows this menu."
  print N "    -u, --user <user>  Execute the command with this user permission. By default '$USERNAME'."
  print N "    -n, --name <name>  Set a custom namespace name. By default '$DEFAULTNAME'."
  print N "    -v, --verbose      Verbose mode."
  print N "    -k, --keep         Don't delete the resolve file after the execution."
  print N "    -s, --shell        Execute a shell."
  print N "    -r, --routing <ip_host> <ip_ns> <netmask>"
  print N "                       Set custom IPs. By default $IPHOSTDEFAULT/$IPNETNSDEFAULT/$NETMASKDEFAULT."
  print N "If command is not passed, the current shell will be executed."
  exit $1
}

# Inside part
# ~~~~~~~~~~~

# This script calls itself. yeah \o/ This part is executed only inside the
# namespace. The arguments are:
# --inside <username> <resolvefile> <silence> <command> <arguments...>
if [ "$1" = "--inside" ]; then
  REALPATH=`realpath $0`
  REALPATH=`dirname $REALPATH`

  shift
  USERNAME="$1"
  shift
  RESOLVEFILE="$1"
  shift
  VERBOSE="$1"
  shift

  print G " * Replacing resolv.conf..."
  mount --bind -o users $RESOLVEFILE /etc/resolv.conf

  print G " * Executing..."

  sudo -u $USERNAME $*
  exit
fi

# The tool
# ~~~~~~~~

# Arguments check
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    # Replacing the name
    -n|--name)
      NAME="$2"
      shift

      if [ "$NAME" = "" ]; then
        printv R "$key requires an argument."
        exit 1
      fi
    ;;

    # Username
    -u|--username)
      USERNAME="$2"
      shift

      if [ "$USERNAME" = "" ]; then
        printv R "$key requires an argument."
        exit 1
      fi
    ;;

    -v|--verbose)
      VERBOSE=y
      ;;

    -k|--keep)
      KEEPRESOLVE=y
      ;;

    -r|--routing)
      IPHOST="$2"
      shift
      IPNETNS="$2"
      shift
      NETMASK="$2"
      shift

      if [ "$IPHOST" = "" ] ||
         [ "$IPNETNS" = "" ] ||
         [ "$NETMASK" = "" ]; then
        printv R "$key requires an 3 arguments."
        exit 1
      fi
      ;;

    -s|--shell)
      set -- $@ "$SHELL"
      ;;

    # Help menu
    -h|--help)
      help_and_exit 0
    ;;

    # The rest
    *)
    break
    ;;
  esac
  shift
done

if [[ $EUID -ne 0 ]]; then
   printv R "$DEFAULTNAME must be run as root."
   exit 1
fi

# No arguments, no party
if [ "$1" = "" ]; then
  help_and_exit 1
fi

# check if network namespace already exists
ip netns list | grep -e ^$NAME\  &> /dev/null
if [ $? -ne 0 ]; then
  printv G "It seems that you don't have the namespace $NAME."
  printvn Y "Do you want to create it? [y/n] "
  read CREATE

  if [ "$CREATE" != y ] && [ "$CREATE" != Y ]; then
    print G "Ok. Bye!"
    exit 0
  fi

  print G " * Creating a $NAME namespace..."
  # add network namespace
  ip netns add $NAME

  # Create veth link.
  print G " * Creating a veth link..."
  ip link add in-$NAME type veth peer name out-$NAME

  # Add out to NS.
  print G " * Sharing the veth interface..."
  ip link set out-$NAME netns $NAME

  ## setup ip address of host interface
  print G " * Setting up IP address of host interface..."
  ip addr add $IPHOST/$NETMASK dev in-$NAME
  ip link set in-$NAME up

  # setup ip address of peer
  print G " * Setting up IP address of peer interface..."
  ip netns exec $NAME ip addr add $IPNETNS/$NETMASK dev out-$NAME
  ip netns exec $NAME ip link set out-$NAME up

  # default route
  print G " * Default routing up..."
  ip netns exec $NAME ip route add default via $IPHOST

  # resolve with tor
  print G " * Resolving via TOR..."
  iptables -t nat -A  PREROUTING -i in-$NAME -p udp --dport 53 -j DNAT --to-destination $IPHOST:5353

  # traffic througth tor
  print G " * Traffic via TOR..."
  iptables -t nat -A  PREROUTING -i in-$NAME -p tcp --syn -j DNAT --to-destination $IPHOST:9040
  iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

  # executing tor
  print G " * Creating the TOR configuration file..."
  TORCONFIGFILE=$(mktemp /tmp/torXXXXXX)
  echo "
    DataDirectory /tmp/torjail-$NAME
    VirtualAddrNetwork $IPNETNS/16
    AutomapHostsSuffixes .onion,.exit 
    AutomapHostsOnResolve 1
    TransPort 9040 
    TransListenAddress $IPHOST
    DNSPort 5353
    DNSListenAddress $IPHOST
    RunAsDaemon 1
    SOCKSPort $IPHOST:9050
  " > $TORCONFIGFILE

  # executing tor
  print G " * Executing TOR..."
  tor --quiet -f $TORCONFIGFILE
else
  print Y "$NAME network namespace already exists!"
fi

RESOLVEFILE=$(mktemp /tmp/resolveXXXXXX)
print G " * Creating a temporary /etc/resolv.conf ($RESOLVEFILE)..."
echo "nameserver $IPHOST" > $RESOLVEFILE

# run your shit
ip netns exec $NAME \
  unshare --ipc --fork --pid --mount --mount-proc \
  $0 --inside "$USERNAME" "$RESOLVEFILE" "$VERBOSE" $*

# removing the resolv.conf file
if [ "$KEEPRESOLVE" != 'y' ]; then
  print G " * Removing the temporary resolve file $RESOLVEFILE..."
  rm "$RESOLVEFILE"
fi

# All done!
